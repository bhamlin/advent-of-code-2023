from collections import defaultdict
from functools import cmp_to_key

MATCH_ORDER = [
    'high', 'pair', 'two', 'three', 'house', 'four', 'five',
]
MATCH_ORDER_DICT = {m: i for i, m in enumerate(MATCH_ORDER)}

FACE_ORDER = list('23456789TJQKA')
FACE_ORDER_DICT = {m: i for i, m in enumerate(FACE_ORDER)}
FACE_ORDER_J = list('J23456789TQKA')
FACE_ORDER_J_DICT = {m: i for i, m in enumerate(FACE_ORDER_J)}


def tokenize(cards):
    tokens = dict()
    # cards.sort(key=lambda x: FACE_ORDER[x])
    cards.sort()
    t = None
    for token in cards:
        if token == t:
            tokens[t] += 1
        else:
            t = token
            tokens[t] = 1
    return tokens


def tokenize_j(cards: list[str]):
    # print(cards)
    tokens = tokenize(list(cards))
    if 'J' in tokens:
        # print('---')
        jokers = tokens['J']
        # hand = {k: v for (k, v) in tokens.items() if not k == 'J'}
        count = 0
        face = None
        for card in [card for card in cards if card != 'J']:
            # print(f' - {card} {cards}')
            if not face:
                face = card
                count = tokens[card]
            else:
                if tokens[card] > count:
                    face = card
                    count = tokens[card]
        # print(cards, jokers, face, count)
        if face:
            del tokens['J']
            tokens[face] += jokers
        # print('tokens', tokens)
        # print('---')
    return tokens


def find_matches(tokens):
    m = list(tokens.values())
    m.sort()
    return m


def type_matches(match_list):
    match_types = ['high']
    if 2 in match_list:
        match_types.append('pair')
    if match_list == [1, 2, 2]:
        match_types.append('two')
    if 3 in match_list:
        match_types.append('three')
    if match_list == [2, 3]:
        match_types.append('two')
        match_types.append('house')
    if 4 in match_list:
        match_types.append('four')
    if 5 in match_list:
        match_types.append('five')

    match_types.sort(key=lambda x: MATCH_ORDER_DICT[x])
    return match_types


def evaluate_hand(hand, *, jokers=False):
    if jokers:
        tokens = tokenize_j(list(hand))
    else:
        tokens = tokenize(list(hand))
    match_list = find_matches(tokens)
    match_types = type_matches(match_list)
    print(f'{hand:5} {match_types[-1]:5} {tokens}')

    return match_types


def best_hand(match_types):
    return match_types[-1]


def rank_hands(game):
    hand_values = list()

    for m in MATCH_ORDER:
        if m in game:
            hand_dict = game[m]
            # hands = list(hand_dict.keys())
            hands = list(map(lambda x: ([FACE_ORDER_DICT[v]
                         for v in list(x[0])], x), hand_dict.items()))
            hands.sort()
            hand_values += [s for _, s in hands]

    return hand_values


def calculate_scores(ranked_hands):
    return [((i+1) * int(v[1])) for i, v in enumerate(ranked_hands)]


model = '''

32T3K 765
T55J5 684
KK677 28
KTJJT 220
QQQJA 483

'''.strip().splitlines()
with open('data/day07-data.txt') as FH:
    model = [line for line in FH.readlines() if 'J' in line][5:7]

# P1
game = defaultdict(dict)
for line in model:
    line = line.strip()
    hand, bid = line.split(' ', 1)
    bid = int(bid)
    best_match = best_hand(evaluate_hand(hand))
    game[best_match][hand] = bid
ranked_hands = rank_hands(game)
scores = calculate_scores(ranked_hands)

print('Problem 1:', sum(scores))

# P2
game = defaultdict(dict)
for line in model:
    line = line.strip()
    hand, bid = line.split(' ', 1)
    bid = int(bid)
    best_match = best_hand(evaluate_hand(hand, jokers=True))
    game[best_match][hand] = bid
ranked_hands = rank_hands(game)
print(ranked_hands)
scores = calculate_scores(ranked_hands)

print('Problem 2:', sum(scores), f'(248256639)')  # 248256639


# import functools

# input_file = open('data/day7-data.txt', 'r')

# buckets = [[], [], [], [], [], [], []]
# total_hands = 0

# for line in input_file:
#     line = line.rstrip('\n').split()

#     hand = line[0]
#     score = int(line[1])

#     cards = {}
#     for c in hand:
#         cards[c] = cards[c] + 1 if c in cards else 1

#     diff_nums = 0
#     max_num = 0
#     jokers = 0
#     for c, n in cards.items():
#         if c == 'J':
#             jokers = n
#         else:
#             diff_nums += 1
#             max_num = max(max_num, n)

#     max_num += jokers

#     bucket = 6
#     if max_num == 5:
#         # 5 of a kind
#         bucket = 0
#     elif max_num == 4:
#         # 4 of a kind
#         bucket = 1
#     elif diff_nums == 2:
#         # Full house
#         bucket = 2
#     elif max_num == 3:
#         # 3 of a kind
#         bucket = 3
#     elif diff_nums == 3:
#         # 2 pairs
#         bucket = 4
#     elif max_num == 2:
#         # 1 pair
#         bucket = 5
#     else:
#         # High card
#         bucket = 6

#     buckets[bucket].append((hand, score))
#     total_hands += 1

# card_values = 'AKQT98765432J'


# def compare(a, b):
#     for i in range(5):
#         ac = card_values.index(a[0][i])
#         bc = card_values.index(b[0][i])
#         if ac < bc:
#             return 1
#         if ac > bc:
#             return -1
#     return 0


# rank = total_hands
# total = 0
# for hands in buckets:
#     hands.sort(key=functools.cmp_to_key(compare), reverse=True)

#     for hand in hands:
#         total += rank * hand[1]
#         rank -= 1

# print(total)
