
model = list(
    map(lambda x: list(
        map(int, x.strip().split())), '''

0 3 6 9 12 15
1 3 6 10 15 21
10 13 16 21 30 45

'''.strip().splitlines()))
with open('../data/day09-data.txt') as FH:
    model = list(
        map(lambda x: list(
            map(int, x.strip().split())), FH))

total = 0
for nums in model:
    n = len(nums)
    # print(nums.count(0), n)
    while nums.count(0) < n:
        total += nums[-1]
        # print(' - ', [nums[i + 1] - nums[i] for i in range(n - 1)])
        nums = [nums[i + 1] - nums[i] for i in range(n - 1)]
        n = len(nums)

print('Part 1:', total)

total = 0
for nums in model:
    n = len(nums)
    # print(nums)
    starts = list()
    while nums.count(0) < n:
        # total += nums[-1]
        starts.append(nums[0])
        # print(' - ', [nums[i + 1] - nums[i] for i in range(n - 1)])
        nums = [nums[i + 1] - nums[i] for i in range(n - 1)]
        n = len(nums)
    starts.reverse()
    # print('-', starts)

    start = 0
    for i in starts:
        start = i - start
    total += start

print('Part 2:', total)
