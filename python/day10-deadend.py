from random import choice


def C(char: str) -> str:
    if char == '.':
        return ' '
    elif char == '|':
        return '│'
    elif char == '-':
        return '─'
    elif char == 'L':
        return '└'
    elif char == 'J':
        return '┘'
    elif char == '7':
        return '┐'
    elif char == 'F':
        return '┌'
    else:
        return char


def line_model(model):
    output = list()
    for line in model:
        row = list()
        for c in line:
            row.append(C(c))
        output.append(row)
    return output


# | is a vertical pipe connecting north and south.
# - is a horizontal pipe connecting east and west.
# L is a 90-degree bend connecting north and east.
# J is a 90-degree bend connecting north and west.
# 7 is a 90-degree bend connecting south and west.
# F is a 90-degree bend connecting south and east.
# . is ground; there is no pipe in this tile.
# S is the starting position of the animal; there is a pipe on this tile, but your sketch doesn't show what shape the pipe has.

P = complex

UP = P(0, 1)
DN = P(0, -1)
LF = P(-1, 0)
RT = P(1, 0)

VALID_MOVES = {
    UP: {'|', 'L', '└', 'J', '┘'},
    DN: {'|', '7', '┐', 'F', '┌'},
    RT: {'-', 'L', '└', 'F', '┌'},
    LF: {'-', '7', '┐', 'J', '┘'},
}


def find_start(model: list[list[str]], start_token='S') -> list[P]:
    output = list()
    for j, line in enumerate(model):
        for n, char in enumerate(line):
            if char == start_token:
                output.append(P(n, j))
    return output


def find_valid_checks_for(pos: complex, *, real_limit: int, imag_limit: int) -> list[complex]:
    '''Assumes 0 is a boundary in both R and j'''
    checks = list()
    r = pos.real
    i = pos.imag
    if r > 0:
        checks.append(LF)
    if r < (real_limit-1):
        checks.append(RT)
    if i > 0:
        checks.append(DN)
    if i < (imag_limit-1):
        checks.append(UP)
    return checks


def get_model_at(model, pos):
    u = int(pos.real)
    v = int(pos.imag)
    return C(model[v][u])


def moves(model, start):
    return {k: v for k, v in {
            pos: C(get_model_at(model, pos))
            for pos in [start + offset for offset in find_valid_checks_for(
                start, real_limit=model_width, imag_limit=model_height)]
            }.items() if v.strip()}


# model = '''

# ..F7.
# .FJ|.
# SJ.L7
# |F--J
# LJ...

# '''.strip().splitlines()
model = '''

..F7.
.FJ|.
FJ.L7
|F--J
LJ...

'''.strip().splitlines()
# with open('../data/day10-data.txt') as FH:
#     model = list(map(lambda x: x.strip(), FH))

model_width = len(model[0])
model_height = len(model)

# n = len(model[0])
# print('╔', '═'*(n+2), '╗', sep='')
# for line in line_model(model):
#     print('║', ''.join(line), '║')
# print('╚', '═'*(n+2), '╝', sep='')

start = [find_start(model) or [P(0, 2)]][0][0]

# print(start, valid_checks)
n = len(model[0])
print('╔', '═'*(n+2), '╗', sep='')
for row, line in enumerate(line_model(model)):
    print(f'{row}', ''.join(line), '║')
print('╚', '═'*(n+2), '╝', sep='')

path = {start}
# print(start)
# print(moves(model, start))
# print(start, get_model_at(model, start))

# pos = P(0, 3)
pos = start
char = get_model_at(model, pos)
# pos, char = choice(list(moves(model, start).items()))
print(pos, get_model_at(model, pos))
while char != 'S':
    path.add(pos)
    print('path', path, pos)
    move_list = [(move, move - pos)
                 for move in moves(model, pos) if not move in path]
    char_list = [[offset, get_model_at(model, move)]
                 for move, offset in move_list]
    print('move', move_list)
    print(char, char_list)
    move_list = [(p, c, c in VALID_MOVES[p])
                 for p, c in char_list]
    move_list = [p for p, _, t, in move_list if t]
    print('move', move_list)
    if len(move_list) > 0:
        move = move_list[0]
        pos += move
        char = get_model_at(model, pos)
        print(pos)
    else:
        break
print(path)
print(len(path)-1)
# while
# print(pos)
# print(C(get_model_at(model, pos[0])))
# print(moves(model, pos), char)
# for valid in valid_checks:
#     pos = pos + valid
#     print('-', pos, C(get_model_at(model, pos)))
