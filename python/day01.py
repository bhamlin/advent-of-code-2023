# --- Day 1: Trebuchet?! ---
###############################################################################
# --- Part One ---
# Something is wrong with global snow production, and you've been selected to
# take a look. The Elves have even given you a map; on it, they've used stars
# to mark the top fifty locations that are likely to be having problems.
#
# You've been doing this long enough to know that to restore snow operations,
# you need to check all fifty stars by December 25th.
#
# Collect stars by solving puzzles. Two puzzles will be made available on each
# day in the Advent calendar; the second puzzle is unlocked when you complete
# the first. Each puzzle grants one star. Good luck!
#
# You try to ask why they can't just use a weather machine ("not powerful
# enough") and where they're even sending you ("the sky") and why your map looks
# mostly blank ("you sure ask a lot of questions") and hang on did you just say
# the sky ("of course, where do you think snow comes from") when you realize
# that the Elves are already loading you into a trebuchet ("please hold still,
# we need to strap you in").
#
# As they're making the final adjustments, they discover that their calibration
# document (your puzzle input) has been amended by a very young Elf who was
# apparently just excited to show off her art skills. Consequently, the Elves
# are having trouble reading the values on the document.
#
# The newly-improved calibration document consists of lines of text; each line
# originally contained a specific calibration value that the Elves now need to
# recover. On each line, the calibration value can be found by combining the
# first digit and the last digit (in that order) to form a single two-digit
# number.
#
# For example:
# 1abc2
# pqr3stu8vwx
# a1b2c3d4e5f
# treb7uchet
#
# In this example, the calibration values of these four lines are 12, 38, 15,
# and 77. Adding these together produces 142.
# Consider your entire calibration document. What is the sum of all of the
# calibration values?
###############################################################################
# --- Part Two ---
# Your calculation isn't quite right. It looks like some of the digits are
# actually spelled out with letters: one, two, three, four, five, six, seven,
# eight, and nine also count as valid "digits".
#
# Equipped with this new information, you now need to find the real first and
# last digit on each line. For example:
#
# two1nine
# eightwothree
# abcone2threexyz
# xtwone3four
# 4nineeightseven2
# zoneight234
# 7pqrstsixteen
#
# In this example, the calibration values are 29, 83, 13, 24, 42, 14, and 76.
# Adding these together produces 281.
#
# What is the sum of all of the calibration values?
###############################################################################

# This is garbage
NUMBERS = {
    'one': 'o1e',
    'two': 't2o',
    'three': 't3e',
    'four': 'f4r',
    'five': 'f5e',
    'six': 's6x',
    'seven': 's7n',
    'eight': 'e8t',
    'nine': 'n9e',
}


def get_digits(input):
    return [c for c in input if c in '0123456789']


def has_number_word(input):
    found = dict()
    for key in NUMBERS.keys():
        if key in input:
            index = input.index(key)
            found[index] = key
    for i in range(len(input)):
        if i in found:
            return found[i]
    return None


def get_word_digits(input):
    number = has_number_word(input)
    while number:
        input = input.replace(number, NUMBERS[number])
        number = has_number_word(input)
    return input


def get_number(input):
    digits = get_digits(input)
    return int(digits[0] + digits[-1])


def get_word_number(input):
    return get_number(get_word_digits(input))


# test_values = [
#     '1abc2',
#     'pqr3stu8vwx',
#     'a1b2c3d4e5f',
#     'treb7uchet',
# ]

with open('data/day01-data.txt') as FH:
    test_values = [line.strip() for line in FH]

total = 0
for input in test_values:
    total += get_number(input)

print('Part 1:', total)

# test_values = [
#     'two1nine',
#     'eightwothree',
#     'abcone2threexyz',
#     'xtwone3four',
#     '4nineeightseven2',
#     'zoneight234',
#     '7pqrstsixteen',
# ]

total = 0
for input in test_values:
    value = get_word_number(input)
    total += value

print('Part 2:', total)
