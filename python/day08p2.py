# model = '''

# RL

# AAA = (BBB, CCC)
# BBB = (DDD, EEE)
# CCC = (ZZZ, GGG)
# DDD = (DDD, DDD)
# EEE = (EEE, EEE)
# GGG = (GGG, GGG)
# ZZZ = (ZZZ, ZZZ)

# '''.strip().splitlines()
# model = '''

# LLR

# AAA = (BBB, BBB)
# BBB = (AAA, ZZZ)
# ZZZ = (ZZZ, ZZZ)

# '''.strip().splitlines()
# model = '''

# LR

# 11A = (11B, XXX)
# 11B = (XXX, 11Z)
# 11Z = (11B, XXX)
# 22A = (22B, XXX)
# 22B = (22C, 22C)
# 22C = (22Z, 22Z)
# 22Z = (22B, 22B)
# XXX = (XXX, XXX)

# '''.strip().splitlines()

from math import lcm


def parse_tree(lines):
    tree = dict()
    for line in lines:
        if '=' in line:
            node, leaves = line.strip().split(' = ', 1)
            leaves = leaves[1:-1].split(', ')
            tree[node] = leaves
    return tree


with open('data/day08-data.txt') as FH:
    model = FH.read().split('\n')

steps = [0 if i == 'L' else 1 for i in model[0]]
step_mod = len(steps)
tree = parse_tree(model[2:])

a_nodes = [node for node in tree.keys() if node.endswith('A')]
# print(a_nodes)

cycles = dict()
for a_node in a_nodes:
    step = 0
    loc = a_node
    while not loc.endswith('Z'):
        loc = tree[loc][steps[step % step_mod]]
        step += 1
    cycles[a_node] = step
print(cycles)
print(lcm(*list(cycles.values())))

# print(steps)
# print(tree)

# loc = 'AAA'
# step_mod = len(steps)
# step = 0
# while loc != 'ZZZ':
#     loc = tree[loc][steps[step % step_mod]]
#     step += 1
# print(loc, step)
