# model = '''

# RL

# AAA = (BBB, CCC)
# BBB = (DDD, EEE)
# CCC = (ZZZ, GGG)
# DDD = (DDD, DDD)
# EEE = (EEE, EEE)
# GGG = (GGG, GGG)
# ZZZ = (ZZZ, ZZZ)

# '''.strip().splitlines()
# model = '''

# LLR

# AAA = (BBB, BBB)
# BBB = (AAA, ZZZ)
# ZZZ = (ZZZ, ZZZ)

# '''.strip().splitlines()

def parse_tree(lines):
    tree = dict()
    for line in lines:
        if '=' in line:
            node, leaves = line.strip().split(' = ', 1)
            leaves = leaves[1:-1].split(', ')
            tree[node] = leaves
    return tree


with open('data/day08-data.txt') as FH:
    model = FH.read().split('\n')

steps = [0 if i == 'L' else 1 for i in model[0]]
print('steps per iteration:', len(steps))

tree = parse_tree(model[2:])

# print(steps)
# print(tree)

loc = 'AAA'
step_mod = len(steps)
step = 0
while loc != 'ZZZ':
    loc = tree[loc][steps[step % step_mod]]
    step += 1
print(loc, step)
