use clap::{Parser, ValueEnum};
use std::{fmt::Display, path::PathBuf};

#[derive(Clone, Debug, Eq, PartialEq, ValueEnum)]
/// Problem identifier for flow control
pub enum Problem {
    One,
    Two,
    All,
}

impl Display for Problem {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let value = match self {
            Self::One => "1",
            Self::Two => "2",
            _ => "all",
        };

        write!(f, "{value}")
    }
}

#[derive(Clone, Debug, Eq, PartialEq, ValueEnum)]
/// Log level stand-in
pub enum LogLevel {
    Debug,
    Info,
}

impl Display for LogLevel {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let value = match self {
            Self::Debug => "debug",
            Self::Info => "info",
        };

        write!(f, "{value}")
    }
}

#[derive(Debug, Parser)]
pub struct Opts {
    /// File to read seed almanac from
    pub input: PathBuf,

    /// Which problem to process
    #[clap(default_value_t=Problem::All)]
    pub problem: Problem,

    /// Log level
    #[clap(short, long, default_value_t=LogLevel::Info)]
    pub log_level: LogLevel,
}

impl Opts {
    pub fn parse_args() -> Opts {
        Self::parse()
    }
}
