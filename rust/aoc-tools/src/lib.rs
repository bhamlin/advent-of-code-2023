pub mod aoc_args;

use anyhow::Result;
use aoc_args::Opts;
use env_logger::{Builder, Env};
use log::Level;
use std::{fs::File, io::BufReader, path::Path};

pub fn aoc_init() -> Opts {
    let opts = Opts::parse_args();
    log_init_by_str(&opts.log_level.to_string());
    opts
}

pub fn log_init_by_str(log_level: &str) {
    Builder::from_env(Env::default().default_filter_or(log_level)).init()
}

pub fn log_init(log_level: Level) {
    log_init_by_str(log_level.as_str())
}

pub fn open_file<T: AsRef<Path>>(file_path: T) -> Result<BufReader<File>> {
    Ok(BufReader::new(File::open(file_path)?))
}
