// #![allow(unused)]

mod data;
mod fileio;
mod sample;

#[allow(unused)]
use crate::sample::{sample_lines_part_1, sample_lines_part_2, sample_lines_part_2_alt};
use anyhow::Result;
use data::StepMap;
use env_logger::{Builder, Env};
use fileio::{load_map, read_file};
use itertools::Itertools;
use lcmx::lcmx;
use log::{debug, info, trace, Level};
use std::{collections::HashMap, time::Instant};

fn main() -> Result<()> {
    Builder::from_env(Env::default().default_filter_or(Level::Info.as_str())).init();

    let (do_part_one, do_part_two, do_part_two_manual) = (false, false, true);
    let use_samples = false;
    let mut map;

    if !use_samples {
        let path = "../../data/day08-data.txt";
        let lines = read_file(path)?;
        map = load_map(lines);
    } else {
        let lines = sample_lines_part_1();
        map = load_map(lines);
    }

    if do_part_one {
        // Part 1
        let time_start = Instant::now();
        let loop_lengths = get_loop_lengths(&map, part_one_starts, part_one_is_a_goal);
        let time_taken = time_start.elapsed();
        info!(target:"day8p1", "- ({:?}) {:?}", time_taken, loop_lengths);
        let time_start = Instant::now();
        let result = part_one(&loop_lengths);
        let time_taken = time_start.elapsed();
        info!(target:"day8p1", "Part 1: {result} ({:?})", time_taken);
    }

    if do_part_two || do_part_two_manual {
        if use_samples {
            // Part 2 samples
            let lines = sample_lines_part_2();
            map = load_map(lines);
        }

        let time_start = Instant::now();
        let loop_lengths = get_loop_lengths(&map, part_two_starts, part_two_is_a_goal);
        let time_taken = time_start.elapsed();
        info!(target:"day8p2", "- ({:?}) {:?}", time_taken, loop_lengths);

        if do_part_two {
            // Part 2 - LCM
            let time_start = Instant::now();
            let result = part_two(&loop_lengths);
            let time_taken = time_start.elapsed();
            info!(target:"day8p2", "Part 2: {result} ({:?})", time_taken);
        }

        if do_part_two_manual {
            // Part 2 - Manually
            let time_start = Instant::now();
            let result = part_two_manually(&loop_lengths);
            let time_taken = time_start.elapsed();
            info!(target:"day8p2hard", "Part 2: {result} ({:?})", time_taken);
        }
    }

    Ok(())
}

#[inline]
fn part_one(loop_lengths: &HashMap<&str, usize>) -> usize {
    loop_lengths.values().sum()
}

#[inline]
fn part_one_starts(keys: Vec<&str>) -> Vec<&str> {
    keys.iter()
        .filter(|&&e| e.eq("AAA".into()))
        .cloned()
        .collect_vec()
}

#[inline]
fn part_one_is_a_goal(state: &str) -> bool {
    state.eq("ZZZ")
}

#[inline]
fn part_two(loop_lengths: &HashMap<&str, usize>) -> usize {
    // loop_lengths.values().fold(1, |a, e| numb)
    let values = loop_lengths.values().cloned().collect_vec();
    let lcm = lcmx(&values).unwrap();
    debug!(target:"part_two", "- ({lcm}) {values:?}");

    lcm
}

#[inline]
fn part_two_starts(keys: Vec<&str>) -> Vec<&str> {
    keys.iter()
        .filter(|&&e| e.ends_with("A"))
        .cloned()
        .collect_vec()
}

#[inline]
fn part_two_is_a_goal(state: &str) -> bool {
    state.ends_with("Z")
}

#[allow(unused)]
fn part_two_manually(loop_lengths: &HashMap<&str, usize>) -> usize {
    // loop_lengths.values().fold(1, |a, e| numb)
    let mut values = loop_lengths
        .values()
        .cloned()
        .map(|v| (v, v))
        .collect::<HashMap<_, _>>();

    while !part_two_manually_test(&values) {
        let max = values.values().max().unwrap().clone();
        for (k, v) in values.iter_mut() {
            if v.clone() < max {
                *v += k;
            }
        }
        // break;
    }

    debug!(target:"part_two_manually", "- {values:?}");

    values.values().max().unwrap().clone()
}

#[inline]
fn part_two_manually_test(state: &HashMap<usize, usize>) -> bool {
    state
        .values()
        .collect_vec()
        .windows(2)
        .all(|e| e[0] == e[1])
}

fn get_loop_lengths(
    map: &StepMap,
    starts: fn(Vec<&str>) -> Vec<&str>,
    is_a_goal: fn(&str) -> bool,
) -> HashMap<&str, usize> {
    let keys = map.step_map.keys().map(|e| e.as_str()).collect_vec();
    let start_keys = starts(keys);
    let mut lengths = HashMap::with_capacity(start_keys.len());
    debug!(target:"get_loop_lengths", "Starts: {start_keys:?}");

    for start_key in start_keys {
        if let Some(len) = get_loop_len(map, start_key, &is_a_goal) {
            lengths.insert(start_key, len);
        }
    }

    lengths
}

fn get_loop_len(map: &StepMap, start_key: &str, is_a_goal: &fn(&str) -> bool) -> Option<usize> {
    let pattern = &map.step_pattern[..];
    let p_len = pattern.len();

    trace!(target:"get_loop_len", "{pattern:?} {p_len}");
    let mut index = 0;
    let mut key = start_key;

    while !is_a_goal(key) {
        let side = pattern.get(index % p_len)?;
        let step = map.step_map.get(key)?;
        key = &step[side.to_usize()];
        trace!(target:"get_loop_len",
            "[{:6}] ({}) {} -> {:?} -> {}",
            index,
            side.to_usize(),
            side,
            step,
            key
        );

        index += 1;
    }

    Some(index)
}
