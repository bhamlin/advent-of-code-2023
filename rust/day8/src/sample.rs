#![allow(unused)]

pub fn sample_lines_part_1() -> Vec<String> {
    let mut output = Vec::with_capacity(9);
    output.push("RL".into());
    output.push("".into());
    output.push("AAA = (BBB, CCC)".into());
    output.push("BBB = (DDD, EEE)".into());
    output.push("CCC = (ZZZ, GGG)".into());
    output.push("DDD = (DDD, DDD)".into());
    output.push("EEE = (EEE, EEE)".into());
    output.push("GGG = (GGG, GGG)".into());
    output.push("ZZZ = (ZZZ, ZZZ)".into());

    output
}

pub fn sample_lines_part_1_alt() -> Vec<String> {
    let mut output = Vec::with_capacity(5);
    output.push("LLR".into());
    output.push("".into());
    output.push("AAA = (BBB, BBB)".into());
    output.push("BBB = (AAA, ZZZ)".into());
    output.push("ZZZ = (ZZZ, ZZZ)".into());

    output
}

pub fn sample_lines_part_2() -> Vec<String> {
    let mut output = Vec::with_capacity(10);
    output.push("LR".into());
    output.push("".into());
    output.push("11A = (11B, XXX)".into());
    output.push("11B = (XXX, 11Z)".into());
    output.push("11Z = (11B, XXX)".into());
    output.push("22A = (22B, XXX)".into());
    output.push("22B = (22C, 22C)".into());
    output.push("22C = (22Z, 22Z)".into());
    output.push("22Z = (22B, 22B)".into());
    output.push("XXX = (XXX, XXX)".into());

    output
}

pub fn sample_lines_part_2_alt() -> Vec<String> {
    let mut output = Vec::with_capacity(10);
    output.push("LR".into());
    output.push("".into());
    output.push("11A = (11B, XXX)".into());
    output.push("11B = (XXX, 11Z)".into());
    output.push("11Z = (11B, XXX)".into());
    output.push("22A = (22B, XXX)".into());
    output.push("22B = (22C, 22C)".into());
    output.push("22C = (22Z, 22Z)".into());
    output.push("22Z = (22B, 22B)".into());
    output.push("33A = (33B, XXX)".into());
    output.push("33B = (33C, 33C)".into());
    output.push("33C = (33D, 33D)".into());
    output.push("33D = (33E, 33E)".into());
    output.push("33E = (33Z, 33Z)".into());
    output.push("33Z = (33B, 33B)".into());
    output.push("XXX = (XXX, XXX)".into());

    output
}
