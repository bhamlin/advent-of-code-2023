// use crate::data::{PreStep, Step, StepMap, LEFT, RIGHT};
use crate::data::{Side, Step, StepMap};
use anyhow::Result;
use itertools::Itertools;
use log::trace;
use std::{
    collections::HashMap,
    fs,
    io::{BufRead, BufReader},
};

pub fn read_file(path: &str) -> Result<Vec<String>> {
    let file = fs::File::open(path)?;
    let reader = BufReader::new(file);
    let lines = reader.lines().map_while(Result::ok).collect();

    Ok(lines)
}

pub fn load_map(lines: Vec<String>) -> StepMap {
    // First line should be a LR pattern
    let step_pattern = lines
        .get(0)
        .unwrap()
        .chars()
        .filter_map(Side::from_char)
        .collect_vec();
    trace!(target:"load_map", "{step_pattern:?}");

    // Lines 2 onward should be a map nodes with a pair of destination nodes
    // Really should verify that the node list we import is viable
    let step_map = lines
        .iter()
        .skip(2)
        .filter(|line| line.contains('='))
        .filter_map(split_line)
        .collect::<HashMap<_, _>>();
    trace!(target:"load_map", "{step_map:?}");

    StepMap {
        step_pattern,
        step_map,
    }
}

fn split_line(input: &String) -> Option<(String, Step)> {
    let (tag, steps) = input.split_once(" = ")?;
    let (leaf_l, leaf_r) = steps[1..9].split_once(", ")?;

    Some((tag.into(), [leaf_l.into(), leaf_r.into()]))
}
