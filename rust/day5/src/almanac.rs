#![allow(unused)]

use crate::{
    rangemap::RangeMap,
    types::{AlmanacDataPoint, AlmanacDataType, SeedDefinition},
};
use anyhow::{Error, Result};
use aoc_tools::open_file;
#[allow(unused)]
use log::{debug, info, trace};
use std::{
    collections::HashMap,
    fmt::Display,
    fs::File,
    io::{BufRead, BufReader},
    path::Path,
};

pub struct Almanac {
    pub seed_ranges: Vec<SeedDefinition>,
    // pub maps: HashMap<AlmanacDataPoint, RangeMap>,
    soil: RangeMap,
    fertilizer: RangeMap,
    water: RangeMap,
    light: RangeMap,
    temperature: RangeMap,
    humidity: RangeMap,
    location: RangeMap,
}

impl Display for Almanac {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let mut seed_ranges = Vec::with_capacity(self.seed_ranges.len());
        for seed_range in self.seed_ranges.iter() {
            seed_ranges.push(seed_range.to_string());
        }

        write!(f, "Almanac: {{Seeds: {}}}", seed_ranges.join(", "))
    }
}

impl Almanac {
    pub fn load_ranges<T: AsRef<Path>>(file_path: T) -> Result<Self> {
        let mut bufreader = open_file(file_path)?;

        let mut first_line = String::new();
        bufreader.read_line(&mut first_line)?;
        let seed_ranges = split_seeds_line_complex(first_line.trim())?;

        Self::load_data(seed_ranges, &mut bufreader)
    }

    pub fn load_naive<T: AsRef<Path>>(file_path: T) -> Result<Self> {
        let mut bufreader = open_file(file_path)?;

        let mut first_line = String::new();
        bufreader.read_line(&mut first_line)?;
        let seed_ranges = split_seeds_line_simple(first_line.trim())?;

        Self::load_data(seed_ranges, &mut bufreader)
    }

    fn load_data(
        seed_ranges: Vec<SeedDefinition>,
        bufreader: &mut BufReader<File>,
    ) -> Result<Self> {
        let mut soil = RangeMap::default();
        let mut fertilizer = RangeMap::default();
        let mut water = RangeMap::default();
        let mut light = RangeMap::default();
        let mut temperature = RangeMap::default();
        let mut humidity = RangeMap::default();
        let mut location = RangeMap::default();

        let lines = bufreader.lines();
        let mut state = AlmanacDataType::None;
        for line in lines.flatten() {
            if line.is_empty() || line.contains(':') {
                state = AlmanacDataType::from(line);
                trace!("State: {state}");
                continue;
            } else {
                trace!("{state}: {line}");
                let parts = line.split(' ').collect::<Vec<_>>();
                if parts.len() < 3 {
                    return Err(Error::msg("Not enough parts"));
                }
                let dest = parts[0].parse::<u128>()?;
                let src = parts[1].parse::<u128>()?;
                let len = parts[2].parse::<u128>()?;
                match state {
                    AlmanacDataType::Soil => soil.add_range(dest, src, len),
                    AlmanacDataType::Fertilizer => fertilizer.add_range(dest, src, len),
                    AlmanacDataType::Water => water.add_range(dest, src, len),
                    AlmanacDataType::Light => light.add_range(dest, src, len),
                    AlmanacDataType::Temperature => temperature.add_range(dest, src, len),
                    AlmanacDataType::Humidity => humidity.add_range(dest, src, len),
                    AlmanacDataType::Location => location.add_range(dest, src, len),
                    _ => (),
                }
            }
        }

        Ok(Self {
            seed_ranges,
            soil,
            fertilizer,
            water,
            light,
            temperature,
            humidity,
            location,
        })
    }

    pub fn get_data_by_seed(&self, seed_id: u128) -> Option<AlmanacDataPoint> {
        let seed = seed_id;
        let soil = self.soil.get_value_at(seed)?;
        let fertilizer = self.fertilizer.get_value_at(soil)?;
        let water = self.water.get_value_at(fertilizer)?;
        let light = self.light.get_value_at(water)?;
        let temperature = self.temperature.get_value_at(light)?;
        let humidity = self.humidity.get_value_at(temperature)?;
        let location = self.location.get_value_at(humidity)?;

        Some(AlmanacDataPoint {
            seed,
            soil,
            fertilizer,
            water,
            light,
            temperature,
            humidity,
            location,
        })
    }
}

fn split_seeds_line_complex(line: &str) -> Result<Vec<SeedDefinition>> {
    let mut list = vec![];
    let seed_ids = &line[7..];
    trace!("{seed_ids}");

    for chunk in seed_ids.split(' ').collect::<Vec<_>>().chunks(2) {
        let id = chunk[0];
        let len = chunk[1];
        let seed_id = id.parse::<u128>()?;
        let seed_range = len.parse::<u128>()?;
        trace!(" - {seed_id} {seed_range}");
        list.push(SeedDefinition {
            seed_id,
            seed_range,
        })
    }

    Ok(list)
    // Err(Error::msg("message"))
}

fn split_seeds_line_simple(line: &str) -> Result<Vec<SeedDefinition>> {
    let mut list = vec![];
    let seed_ids = &line[7..];
    trace!("{seed_ids}");

    for id in seed_ids.split(' ') {
        let seed_id = id.parse::<u128>()?;
        trace!(" - {seed_id}");
        list.push(SeedDefinition {
            seed_id,
            seed_range: 1,
        })
    }

    Ok(list)
}
