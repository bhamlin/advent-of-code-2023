#![allow(dead_code)]

mod almanac;
mod rangemap;
mod types;

use std::path::Path;

use almanac::Almanac;
use anyhow::Result;
use aoc_tools::{aoc_args::Problem, aoc_init};
#[allow(unused)]
use log::{debug, info, warn};

fn main() -> Result<()> {
    let opts = aoc_init();
    debug!("{opts:?}");

    if opts.problem == Problem::All || opts.problem == Problem::One {
        let result = part1(&opts.input)?;
        warn!("Problem 1: {result}");
    }
    if opts.problem == Problem::All || opts.problem == Problem::Two {
        let result = part2(&opts.input)?;
        warn!("Problem 2: {result}");
    }

    Ok(())
}

#[allow(unused)]
fn part1<T: AsRef<Path>>(input: T) -> Result<u128> {
    let almanac = Almanac::load_naive(&input)?;
    debug!("{almanac}");

    find_minimum(&almanac)
}

#[allow(unused)]
fn part2<T: AsRef<Path>>(input: T) -> Result<u128> {
    let almanac = Almanac::load_ranges(&input)?;
    debug!("{almanac}");

    find_minimum(&almanac)
}

fn find_minimum(almanac: &Almanac) -> Result<u128> {
    let mut location = u128::MAX;
    for seed_range in almanac.seed_ranges.iter() {
        debug!(" -> {}", seed_range.seed_id);
        for seed in seed_range.iter() {
            let data = almanac.get_data_by_seed(seed).unwrap();
            location = location.min(data.location);
        }
    }

    Ok(location)
}
