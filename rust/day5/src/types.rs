use std::fmt::Display;

#[derive(Debug, Default)]
pub struct AlmanacDataPoint {
    pub seed: u128,
    pub soil: u128,
    pub fertilizer: u128,
    pub water: u128,
    pub light: u128,
    pub temperature: u128,
    pub humidity: u128,
    pub location: u128,
}

#[derive(Clone, Eq, Hash, PartialEq)]
pub enum AlmanacDataType {
    None,
    Seed,
    Soil,
    Fertilizer,
    Water,
    Light,
    Temperature,
    Humidity,
    Location,
}

impl Display for AlmanacDataType {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let text = match self {
            Self::None => "None",
            Self::Seed => "Seed",
            Self::Soil => "Soil",
            Self::Fertilizer => "Fertilizer",
            Self::Water => "Water",
            Self::Light => "Light",
            Self::Temperature => "Temperature",
            Self::Humidity => "Humidity",
            Self::Location => "Location",
        };

        write!(f, "{text}")
    }
}

impl From<String> for AlmanacDataType {
    fn from(value: String) -> Self {
        match value.as_str() {
            "seed-to-soil map:" => AlmanacDataType::Soil,
            "soil-to-fertilizer map:" => AlmanacDataType::Fertilizer,
            "fertilizer-to-water map:" => AlmanacDataType::Water,
            "water-to-light map:" => AlmanacDataType::Light,
            "light-to-temperature map:" => AlmanacDataType::Temperature,
            "temperature-to-humidity map:" => AlmanacDataType::Humidity,
            "humidity-to-location map:" => AlmanacDataType::Location,
            _ => AlmanacDataType::Seed,
        }
    }
}

pub struct SeedDefinition {
    pub seed_id: u128,
    pub seed_range: u128,
}

impl Display for SeedDefinition {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self.seed_range {
            0 => write!(f, "0"),
            1 => write!(f, "{}", self.seed_id),
            _ => write!(f, "[{},{})", self.seed_id, self.seed_id + self.seed_range),
        }
    }
}

impl SeedDefinition {
    pub fn iter(&self) -> impl Iterator<Item = u128> {
        let s = self.seed_id;
        let r = self.seed_range;

        s..s + r
    }
}
