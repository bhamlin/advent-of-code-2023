use std::{collections::HashMap, fmt::Display};

pub struct Range {
    pub start: u128,
    pub length: u128,
}

#[derive(Default)]
pub struct RangeMap {
    maps: HashMap<u128, Range>,
    chunks: Vec<u128>,
}

impl Display for RangeMap {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        for src in self.chunks.iter() {
            let range = &self.maps[src];
            let s_end = src + range.length - 1;
            let d_end = range.start + range.length - 1;
            write!(f, "[{src}..{s_end}] -> [{}..{d_end}]", range.start)?;
        }

        Ok(())
    }
}

impl RangeMap {
    pub fn add_range(&mut self, dest: u128, src: u128, len: u128) {
        self.maps.insert(
            src,
            Range {
                start: dest,
                length: len,
            },
        );
        self.update_chunks();
    }

    pub fn get_value_at(&self, index: u128) -> Option<u128> {
        let mut offset = 0;
        let mut start = index;
        for &src in self.chunks.iter() {
            if src > index {
                break;
            } else {
                let range = self.maps.get(&src)?;
                if index >= (src + range.length) {
                    continue;
                } else {
                    offset = index - src;
                    start = range.start
                }
            }
        }
        Some(start + offset)
    }

    fn update_chunks(&mut self) {
        self.chunks.clear();
        for key in self.maps.keys() {
            self.chunks.push(*key);
        }
        self.chunks.sort();
    }
}
