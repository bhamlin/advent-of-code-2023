use std::io::BufRead;

use anyhow::Result;
use aoc_tools::{aoc_args::Problem, aoc_init, open_file};
#[allow(unused)]
use log::{debug, info};

#[derive(Debug)]
struct RaceSet {
    pub time: u128,
    pub distance: u128,
}

type RaceList = Vec<RaceSet>;

fn main() -> Result<()> {
    let opts = aoc_init();
    debug!("{opts:?}");

    let mut times = String::new();
    let mut dists = String::new();
    let mut fh = open_file(opts.input)?;
    fh.read_line(&mut times)?;
    fh.read_line(&mut dists)?;

    if (opts.problem == Problem::All) || (opts.problem == Problem::One) {
        info!("-------------");
        info!("Problem 1");
        let times = parse_input(close_spaces(times[9..].trim()));
        let dists = parse_input(close_spaces(dists[9..].trim()));

        debug!("Times: {times:?}");
        debug!("Dists: {dists:?}");

        let races = parse_races(times, dists);
        debug!("Races: {races:?}");
        let result = run_races(races);
        info!("Result: {result}");
        info!("-------------");
    }

    if (opts.problem == Problem::All) || (opts.problem == Problem::Two) {
        info!("-------------");
        info!("Problem 2");
        let times = parse_input(no_spaces(times[9..].trim()));
        let dists = parse_input(no_spaces(dists[9..].trim()));

        debug!("Times: {times:?}");
        debug!("Dists: {dists:?}");

        let races = parse_races(times, dists);
        debug!("Races: {races:?}");
        let race = races.get(0).unwrap();
        let result = use_calculus_instead(race.time, race.distance);
        // let result = get_race_choice_count(races.get(0).unwrap());
        info!("Result: {result}");
        info!("-------------");
    }

    Ok(())
}

fn use_calculus_instead(time: u128, dist: u128) -> u128 {
    let b = time as f64;
    let c = 0.0 - dist as f64;
    let x = (b - ((b * b) + (4.0 * c)).sqrt()) / 2.0;

    let min_time = x.floor() as u128;

    time - ((min_time * 2) + 1)
}

fn model_boat(hold: u128, max: u128) -> u128 {
    let mut distance = 0;
    for step in 1..=max {
        if step > hold {
            distance += hold
        }
    }
    distance
}

fn get_race_choice_count(race: &RaceSet) -> u128 {
    let mut choices = 0;
    for hold in 1..race.time {
        let distance = model_boat(hold, race.time);
        if distance > race.distance {
            choices += 1;
        }
    }
    choices
}

fn run_races(races: RaceList) -> u128 {
    let mut total = 1;

    for race in races {
        total *= get_race_choice_count(&race);
    }

    total
}

fn parse_races(times: Vec<u128>, dists: Vec<u128>) -> RaceList {
    times
        .iter()
        .cloned()
        .zip(dists)
        .map(|(time, distance)| RaceSet { time, distance })
        .collect()
}

#[inline]
fn no_spaces(input: &str) -> String {
    input.replace(' ', "")
}

#[inline]
fn close_spaces(input: &str) -> String {
    input.replace("  ", " ")
}

#[inline]
fn parse_input(input: String) -> Vec<u128> {
    input.split(' ').flat_map(|i| i.parse::<u128>()).collect()
}
